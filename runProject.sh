#!/bin/sh


echo "Compiling..."
scalac projet.scala
echo "Testing with good example (should work)"
sleep 1
scala Tondeuse scalaEX.txt
sleep 1
echo "Testing with wrong lines (should not work)"
sleep 1
scala Tondeuse scalaBadEx.txt
sleep 1
echo "Testing with something written after good data (should work)"
sleep 1
scala Tondeuse scalaBadEx2.txt
sleep 1
echo "Testing with 3 mowes (should work)"
sleep 1
scala Tondeuse scalaEX2.txt
while true; do
echo "Enter the name of the file you want to test the program with : "
read namefile
echo "Testing with "$namefile
scala Tondeuse $namefile
done
exit
//N => (x,y+1)
//S => (x, y-1)
//E => (x+1, y)
//W => (x-1, y)
import scala.io.Source
import java.nio.file.{Paths, Files}
object Tondeuse{
	sealed trait Direction
	case object N extends Direction
	case object S extends Direction
	case object E extends Direction
	case object W extends Direction

	sealed trait Move
	case object A extends Move
	case object G extends Move
	case object D extends Move

	def main(args: Array[String]) :  Unit = {
		if(args.length == 0){
			println("Need file name as an argument")
			return
		}
		if(!Files.exists(Paths.get(args(0)))) {
			println("This file does not exist")
			return
		}
		val f = Source.fromFile(args(0)).getLines.toList
		println(f.mkString("\n")+"\ngives : \n")
		val size_of_rect = f(0)

		f match {
			case rect::pos::instruct::tail =>
				size_of_rect.split(" ").toList match {
					case maxx::maxy::any => //si l'utilisateur met des choses après la taille du rectangle, on les ignore
						val maxX = getIntFromString(maxx);
						val maxY = getIntFromString(maxy);
						pos.split(" ").toList match {
						case x::y::d::any => //si l'utlisateur met des choses après les 3 paramètres d'initialisation, on les ignore
							val initX = getIntFromString(x);
							val initY = getIntFromString(y)
							val initDir = fromStringDirection(d);						
							val tuple = (initX, initY, initDir, maxX, maxY)
							tuple match {
								case (Some(ix), Some(iy), Some(d), Some(mx), Some(my))=>
									val listMove = fromListToListMove(instruct.toList, List[Move]()).reverse 
									val result = recurs(mx, my, ix, iy, d, listMove, tail, "")
									result match {
										case Some(x) => println(x)
										case None => println("incorrect file : cannot get result")
									}
								case _ => 
									println("incorrect file : one of the parameter is not a int") 
							}
							
						case _ =>
						println("incorrect file : second line is not like \"int int direction\"")
					}
					case _ =>
						println("incorrect file : first line is not like \"int int\"")
				}
				
			case _ => println("incorrect file : incorrect file skeletton")
				
		}	
	}
	def isAllDigits(x: String) = (x forall Character.isDigit) && x.length != 0

	def fromStringDirection(str: String) : Option[Direction] = {
		str match {
			case "N" => Some(N)
			case "S" => Some(S)
			case "E" => Some(E)
			case "W" => Some(W)
			case _ => None
		}
	}

	def fromListToListMove(list : List[Char], toReturn : List[Move]) : List[Move] = { //On ignore les charactères incorrects. Si l'utilisateur n'a entré que de mauvais mouvements, il aura une liste vide.
		list match {
			case c::tail => c match {
				case 'A' => fromListToListMove(tail, A::toReturn)
				case 'G' => fromListToListMove(tail, G::toReturn)
				case 'D' => fromListToListMove(tail, D::toReturn)
				case _ => fromListToListMove(tail, toReturn)
			}
			case _ => {
				toReturn
			}
		}
	}

	def getIntFromString(str: String) : Option[Int] = {
		str match {
			case x => if(isAllDigits(x)) Some(x.toInt) else None
		}
	}

	def recurs(maxX:Int, maxY: Int, x:Int, y:Int, orient:Direction, move:List[Move], rest:List[String], posFinal:String): Option[String] = {
			move match {
			case A::tail => orient match {
				case N => if(y+1 <= maxY) recurs(maxX, maxY, x, y+1, orient, tail,rest,posFinal ) else recurs(maxX, maxY, x, y, orient, tail,rest,posFinal)
				case S => if(y-1 >=0) recurs(maxX, maxY, x, y-1, orient, tail,rest,posFinal) else recurs(maxX, maxY, x, y, orient, tail, rest, posFinal)
				case E => if(x+1 <= maxX) recurs(maxX, maxY, x+1, y, orient, tail, rest, posFinal) else recurs(maxX, maxY, x, y, orient, tail, rest, posFinal)
				case W => if(x-1 >=0) recurs(maxX, maxY, x-1, y, orient, tail, rest, posFinal) else recurs(maxX, maxY, x, y, orient, tail, rest, posFinal)
				case _ => None
			}
			case D::tail => orient match {
				case N => recurs(maxX, maxY, x,y,E, tail, rest, posFinal)
				case S => recurs(maxX, maxY, x,y,W, tail, rest, posFinal)
				case E => recurs(maxX, maxY, x,y,S, tail, rest, posFinal)
				case W => recurs(maxX, maxY, x,y,N, tail, rest, posFinal)
				case _ => None
			}
			case G::tail => orient match {
				case N => recurs(maxX, maxY, x,y,W, tail, rest, posFinal)
				case S => recurs(maxX, maxY, x,y,E, tail, rest, posFinal)
				case E => recurs(maxX, maxY, x,y,N, tail, rest, posFinal)
				case W => recurs(maxX, maxY, x,y,S, tail, rest, posFinal)
				case _ => None
			}
			case _ => {

					val newPosFinal = posFinal.concat(x+" "+y+" "+orient+"\n")
					rest match {
						case pos::instruct::tail =>
						pos.split(" ").toList match {
							case x::y::d::any => 
								val initX = getIntFromString(x);
								val initY = getIntFromString(y)
								val initDir = fromStringDirection(d);
								initDir match {
									case Some(initD) =>
										initY match {
											case Some(iy) =>
												initX match {
													case Some(ix) =>
														val listMove = fromListToListMove(instruct.toList, List[Move]()).reverse
															recurs(maxX, maxY, ix, iy, initD, listMove, tail, newPosFinal)
													case None => Some(newPosFinal + "incorrect file : cannot get initX in RECURS")//on s'arrete quand on trouve une ligne incorrecte
												}
											case None => Some(newPosFinal +"incorrect file : cannot get initY in RECURS")
										}
									case None => Some(newPosFinal +"incorrect file : cannot get init direction in RECURS")
								}
							case _ =>
								Some(newPosFinal + "incorrect file : second line is not like \"int int direction\" in RECURS")
						}
							
						case _ => Some(newPosFinal)
					}
				}
			}
	}

}

